import json
import random
import subprocess
import pymongo
from pymongo import MongoClient

client = MongoClient(host='localhost',
                     port=27017,
                     username='root',
                     password='example',
                     )
db = client['vgame']
orders = db.commandes
posts = orders.posts


def runscript(cmd, command_number):
    i = 0
    id = 0
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    while i < command_number:
        line = p.stdout.readline().decode("utf-8")
        command = json.loads(line)
        print(command)
        post_id = posts.insert_one(command).inserted_id
        print(post_id)
        if line == '' and p.poll() != None:
            break
        i += 1
    p.kill()


def run():
    while True:
        interval = round(random.uniform(50, 10000))
        command_number = round(random.uniform(1, 200))
        print('interval: {} - commands: {}'.format(interval, command_number))
        runscript('php ../data/script.php {}'.format(interval), command_number)


if __name__ == "__main__":
    run()
