#!/bin/bash

MONGO_HOST="localhost"
MONGO_DATABASE="vgame"
MONGO_USERNAME="root"
MONGO_PWD="example"

APP_NAME="EscapeGame"

TIMESTAMP=`date +%F-%H%M`

BACKUP_NAME="$APP_NAME-$TIMESTAMP"
BACKUPS_DIR="/home/dump/$APP_NAME/$BACKUP_NAME"


mkdir -p $BACKUPS_DIR

mongodump --host $MONGO_HOST --port 27017 -d $MONGO_DATABASE --collection commandes.posts  --authenticationDatabase "admin" -u $MONGO_USERNAME -p $MONGO_PWD -o $BACKUPS_DIR

tar -zcvf $BACKUPS_DIR.tgz $BACKUPS_DIR
rm -rf $BACKUPS_DIR

# cron
# every 1:45 am
# 45 1 * * * ../../scripts/mongo_backup.sh